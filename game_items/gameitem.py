from game_items.vector import Vector
from game_items.colors import Colors
from abc import ABC


class GameItem(ABC):
    def __init__(self, size, color_index, x_coordinate, y_coordinate):
        self.vector = Vector(x_coordinate, y_coordinate)
        self.size = size
        self.color = Colors(color_index)
