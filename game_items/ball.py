from game_items.gameitem import GameItem


class BallObj(GameItem):
    def __init__(self, ball_speed, ball_size, ball_y_coordinate, ball_color):
        super().__init__(ball_size, ball_color, 199, ball_y_coordinate)
        self.speed = ball_speed
