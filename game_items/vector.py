class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def move(self, other_vector):
        """
        :param other_vector: x and y coordinates we want to add/sub to our vector coordinates.
        """
        self.x += other_vector.x  # update the vector x coordinate.
        self.y += other_vector.y  # update the vector y coordinate.

    def __sub__(self, other_vector):
        """
        This function checks if the bird touch another ball.
        :param other_vector: x and y coordinates of the detected ball.
        :return: new vector that will be send to abs function.
        """
        # in case both items are vectors.
        if isinstance(other_vector, Vector):
            x = self.x - other_vector.x
            y = self.y - other_vector.y
        # in case one item is vector and the other is number.
        else:
            x = self.x - other_vector
            y = self.y - other_vector
        return Vector(x, y)

    def __abs__(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5  # calculate the distance between the bird and specific ball.