from game_items.gameitem import GameItem
from game_items.vector import Vector


class Bird(GameItem):
    def __init__(self):
        super().__init__(15, 1, 0, 0)
        self.score = 0

    def bird_move(self, x, y):
        """
        :param x: how many pixels to move in the x direction
        :param y: how many pixels to move in the y direction
        """
        # GAME_STATE['score'] += 1  # increase the score in each key press(up, left or right).
        self.score += 1  # increase the score in each key press(up, left or right).
        move_vector = Vector(x, y)
        self.vector.move(move_vector)
