from turtle import Turtle, clear, goto, dot, update, ontimer
from random import randint, randrange
from game_items.ball import BallObj


class Game:
    def __init__(self, bird):
        self.bird = bird
        self.draw_to_screen = Turtle(visible=False)  # in order to make the cursor invisible.
        self.balls_array = []

    def inside(self, point):
        "Return True if point on screen."
        return -200 < point.x < 200 and -200 < point.y < 200

    def draw(self, alive):
        "Draw screen objects."
        clear()

        goto(self.bird.vector.x, self.bird.vector.y)

        # in case game is still on.
        if alive:
            self.draw_to_screen.undo()  # delete the score from screen.
            self.draw_to_screen.color("green")
            self.draw_to_screen.write("Score: {}".format(self.bird.score))  # prints to the screen the updated score.
            dot(15, 'black')

        # in case game is over
        else:
            self.draw_to_screen.undo()  # delete the score from screen.
            self.draw_to_screen.color("red")  # change the text color to red.
            # prints to the screen the final score.
            self.draw_to_screen.write("Game finished with Score: {}".format(self.bird.score))
            dot(15, 'red')
            for ball in self.balls_array:  # delete all balls
                ball.size = 0

        for ball in self.balls_array:
            goto(ball.vector.x, ball.vector.y)
            dot(ball.size, ball.color.color)

        update()

    def move(self):
        "Update object positions."
        self.bird.vector.y -= 5
        for ball in self.balls_array:
            ball.vector.x -= ball.speed

        if randrange(10) == 0:
            y = randrange(-199, 199)  # randomize ball initialized y coordinate.
            size = randint(10, 50)  # randomize ball size.
            speed = randint(3, 10)  # randomize ball speed.
            color = randint(0, 6)
            ball = BallObj(speed, size, y, color)  # initialize ball object.
            self.balls_array.append(ball)  # add ball to balls array

        while len(self.balls_array) > 0 and not self.inside(self.balls_array[0].vector):
            self.balls_array.pop(0)

        if not self.inside(self.bird.vector):
            self.draw(False)
            return

        for ball in self.balls_array:
            if abs(ball.vector - self.bird.vector) < 15:
                self.draw(False)
                return

        self.draw(True)
        ontimer(self.move, 50)
