from game_items.vector import Vector
from game_items.ball import BallObj

def test_vector_creation():
    vector_to_test = Vector(10, 10)
    assert vector_to_test.x == 10
    assert vector_to_test.y == 10


def test_vector_move_function():
    vector_to_test = Vector(10, 10)
    other_vector = Vector(20, 20)
    vector_to_test.move(other_vector)
    assert vector_to_test.x == 30
    assert vector_to_test.y == 30

    vector_to_test = Vector(10, 10)
    other_vector = Vector(-5, 0)
    vector_to_test.move(other_vector)
    assert vector_to_test.x == 5
    assert vector_to_test.y == 10


def test_vector_sub_function():
    # in case both items are vectors
    first_vector = Vector(10, 5)
    second_vector = Vector(5, 4)
    vector_to_test = first_vector - second_vector
    assert vector_to_test.x == 5
    assert vector_to_test.y == 1

    # in case one item is a vector and second item is number
    first_vector = Vector(10, 5)
    number = 5
    vector_to_test = first_vector - number
    assert vector_to_test.x == 5
    assert vector_to_test.y == 0


def test_vector_abs_function():
    vector_to_test = Vector(6, 8)
    assert vector_to_test.__abs__() == 10


def test_ball_object_init():
    ball = BallObj(10, 20, 30, 0)
    assert ball.size == 20
    assert ball.speed == 10
    assert ball.vector.y == 30
    assert ball.color.color == "red"



