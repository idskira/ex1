from turtle import setup, hideturtle, up, tracer, listen, onkeypress, done
from game_items.bird import Bird
from games.flappy_game import Game

if __name__ == '__main__':
    setup(420, 420, 900, 300)
    bird = Bird()
    game = Game(bird)
    hideturtle()
    up()
    tracer(False)
    # listen to key press - up, left and right keys.
    listen()
    onkeypress(lambda: bird.bird_move(10, 0), "Right")
    onkeypress(lambda: bird.bird_move(-10, 0), "Left")
    onkeypress(lambda: bird.bird_move(0, 30), "Up")
    game.move()
    done()
